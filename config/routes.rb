Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
  get 'welcome/index'  
  get 'welcome/authorize', to: 'welcome#authorize'
  get 'welcome/authorized', to: 'welcome#authorized'
  
  get '/user_recent_media', to: 'insta_users#recent_media'
  get '/imageEnlarge', to: 'insta_users#enlarge_img'
end
