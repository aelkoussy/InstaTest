class WelcomeController < ApplicationController
  def index
    @demo_pics = ["https://images.unsplash.com/photo-1472162072942-cd5147eb3902?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0c96f8e52a9496fb9e6cee3577e4e3cf&auto=format&fit=crop&w=1950&q=80",
      "https://images.unsplash.com/photo-1504022462188-88f023db97bf?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5af65afcc221ab936ff25a2098e42f26&auto=format&fit=crop&w=1950&q=80",
      "https://images.unsplash.com/photo-1503346463167-72fcaee0ecf2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=602da4c8715996b6aac1d4d1f4bb825a&auto=format&fit=crop&w=1950&q=80",
      "https://images.unsplash.com/photo-1501901609772-df0848060b33?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=cd48efc0a98b81f0dfad2ef9813370b8&auto=format&fit=crop&w=1950&q=80",
      "https://images.unsplash.com/photo-1466193341027-56e68017ee2d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c9a5b750e07819aacb7942b8e552dbda&auto=format&fit=crop&w=1950&q=80",
      "https://images.unsplash.com/photo-1498568715259-5c1dc96aa8e7?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad416811db9057a089db51b5b7f97794&auto=format&fit=crop&w=1950&q=80"
    ]
  end

  def authorize
    # redirect to instagram oauth2 endpoint to get the code
     redirect_to Instagram.authorize_url(:redirect_uri => CALLBACK_URL)
  end

  def authorized
    # get access token for instagram
    response = Instagram.get_access_token(params[:code], :redirect_uri => CALLBACK_URL)

    # save access token in session 
    session[:access_token] = response.access_token
    redirect_to :controller => 'insta_users', :action => 'recent_media' 
    
  end

end
