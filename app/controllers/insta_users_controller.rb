class InstaUsersController < ApplicationController
    def recent_media
        client = Instagram.client(:access_token => session[:access_token])
        @user = client.user
        @pictures = client.user_recent_media
    end

    def enlarge_img
        @img = params[:img]
        @back = 'javascript:history.back()'
    end
end
